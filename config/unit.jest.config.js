module.exports = {
  rootDir: '../',
  collectCoverage: true,
  collectCoverageFrom: ['src/**.{js,js}', '!src/index.js'],
  coverageReporters: ['text'],
  testEnvironment: 'node'
};
