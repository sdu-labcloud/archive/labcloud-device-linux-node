// eslint-disable-next-line import/no-unresolved,import/no-extraneous-dependencies
const ws281x = require('rpi-ws281x-native');
const color = require('./color');

const ledCount = 1;
const leds = new Uint32Array(ledCount);
let animationName = 'static.black';
let animationInterval = null;
let currentColor = color('black');

ws281x.init(ledCount, {
  dmaNum: 10
});

const render = colorName => {
  currentColor = color(colorName);
  leds[0] = currentColor;
  ws281x.render(leds);
};

const resetAnimation = newAnimationName => {
  if (animationName !== newAnimationName) {
    if (animationInterval) {
      clearInterval(animationInterval);
      animationInterval = null;
    }
    animationName = newAnimationName;
    return true;
  }
  return false;
};

exports.static = colorName => {
  const name = `static.${colorName}`;
  if (resetAnimation(name)) {
    render(colorName);
  }
};

exports.blink = (colorNameA, colorNameB, frequencyHz) => {
  const name = `blink.${colorNameA}.${colorNameB}.${frequencyHz}`;
  if (resetAnimation(name)) {
    const updateAnimation = () => {
      if (currentColor === color(colorNameA)) {
        render(colorNameB);
      } else {
        render(colorNameA);
      }
    };
    updateAnimation();
    animationInterval = setInterval(updateAnimation, 1000 / (frequencyHz * 2));
  }
};

exports.terminate = () => {
  ws281x.reset();
};
