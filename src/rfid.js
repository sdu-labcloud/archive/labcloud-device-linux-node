const { promisify } = require('util');
const cp = require('child_process');
const logger = require('./logger');
const { delay } = require('./util');

const execFile = promisify(cp.execFile);
const rfidToolBinary = '/usr/bin/rfidb1-tool';
const serialPort = '/dev/serial0';

const runTool = async (...args) => {
  try {
    const streams = await execFile(...args);
    return streams;
  } catch (err) {
    // Ignore "Command failed" error as the rfidb1-tool
    // will always return exit code 255.
    if (!/^Command failed:/.test(err.message)) {
      logger.error(err.message);
      return {};
    }
    return {
      stdout: err.stdout,
      stderr: err.stderr
    };
  }
};

exports.getUid = async () => {
  // Execute tool twice as it will return nothing every second time.
  for (let i = 0; i < 2; i += 1) {
    // eslint-disable-next-line no-await-in-loop
    const streams = await runTool(rfidToolBinary, [serialPort, 'uid']);
    if (streams.stdout) {
      const uidLine = streams.stdout
        .split('\n')
        .find(line => /^UID:/.test(line));
      if (uidLine) {
        return uidLine.split(':')[1].trim();
      }
    }
  }
  return '';
};

exports.isTagRemovedOrChanged = async (initialTagId, allowedFailures = 8) => {
  if (!initialTagId) {
    return initialTagId;
  }
  let failures = 0;
  let currentTagId = await exports.getUid();
  while (failures < allowedFailures) {
    if (currentTagId !== initialTagId) {
      failures += 1;
    } else {
      failures = 0;
    }
    // eslint-disable-next-line no-await-in-loop
    await delay(500);
    // eslint-disable-next-line no-await-in-loop
    currentTagId = await exports.getUid();
  }
  return currentTagId;
};

exports.readDataBlocks = async (startBlockAddress, blockCount) => {
  const command = 'rdblock';
  const storageBufferOffset = 0;
  const keyName = 'B';
  const key = 'FFFFFFFFFFFF';
  const args = [
    serialPort,
    command,
    startBlockAddress,
    blockCount,
    storageBufferOffset,
    keyName,
    key
  ];
  const streams = await runTool(rfidToolBinary, args);
  if (streams.stdout) {
    const dataLine = streams.stdout
      .split('\n')
      .find(line => /^Data bytes:/.test(line));
    if (dataLine) {
      return dataLine.split(':')[1].trim();
    }
  }
  return '';
};

exports.writeDataBlocks = async (startBlockAddress, blockCount) => {
  const command = 'wdblock';
  const storageBufferOffset = 0;
  const keyName = 'B';
  const key = 'FFFFFFFFFFFF';
  const args = [
    serialPort,
    command,
    startBlockAddress,
    blockCount,
    storageBufferOffset,
    keyName,
    key
  ];
  const streams = await runTool(rfidToolBinary, args);
  return !/communication error/.test(streams.stdout);
};

exports.writeDataBuffers = async data => {
  const command = 'wdata';
  const storageBufferOffset = 0;
  const args = [serialPort, command, storageBufferOffset, data];
  const streams = await runTool(rfidToolBinary, args);
  return /write OK/.test(streams.stdout);
};
