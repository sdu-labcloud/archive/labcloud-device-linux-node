const { spawn } = require('child_process');
const Mp4Fragmenter = require('mp4frag');
const logger = require('./logger');

const bitrate = 10e6;
const resolution = { width: 1280, height: 720 };
const framerate = 30;
const keyframeFrequency = 0.5;
const sourceCli = 'raspivid';
const sourceCliOptions = [
  '--raw-format',
  'rgb',
  '--codec',
  'H264',
  '--profile',
  'high',
  '--level',
  '4.2',
  '--framerate',
  framerate,
  '--intra',
  Math.round(framerate / keyframeFrequency),
  '--vflip',
  '--hflip',
  '--timeout',
  '0',
  '--width',
  resolution.width,
  '--height',
  resolution.height,
  '--bitrate',
  bitrate,
  '--annotate',
  '12',
  '--output',
  '-'
];
const muxCli = 'ffmpeg';
const muxCliOptions = [
  '-hide_banner',
  '-framerate',
  framerate,
  '-i',
  'pipe:0',
  '-an',
  '-c',
  'copy',
  '-movflags',
  'empty_moov+omit_tfhd_offset+frag_keyframe+default_base_moof',
  '-f',
  'mp4',
  'pipe:1'
];
const stream = {
  source: null,
  mux: null,
  fragmenter: null,
  socket: null,
  isStreaming: false
};
const capabilityEventPath = '/nodes/capabilities/raspberry_pi_camera';

const stop = (err = null) => {
  if (err) {
    logger.error(err);
  }

  if (stream.mux && stream.fragmenter) {
    stream.mux.stdout.unpipe(stream.fragmenter);
  }

  if (stream.source && stream.mux) {
    stream.source.stdout.unpipe(stream.mux.stdin);
  }

  if (stream.mux) {
    logger.info('Stopping muxer.');
    stream.mux.kill('SIGKILL');
    stream.mux = null;
  }

  if (stream.source) {
    logger.info('Stopping camera.');
    stream.source.kill('SIGKILL');
    stream.source = null;
  }

  stream.fragmenter = null;

  if (stream.isStreaming) {
    logger.info('Stopping video stream.');
    stream.isStreaming = false;
  }
};

const start = () => {
  if (stream.isStreaming) {
    return stream;
  }

  logger.info('Starting video stream.');

  stream.source = spawn(sourceCli, sourceCliOptions);
  stream.mux = spawn(muxCli, muxCliOptions);
  stream.fragmenter = new Mp4Fragmenter({ bufferListSize: 2 });

  stream.source.stderr.on('data', data => {
    logger.debug(data.toString());
  });

  stream.mux.stderr.on('data', data => {
    logger.debug(data.toString());
  });

  stream.fragmenter.on('initialized', object => {
    stream.socket.emit(`${capabilityEventPath}/mime`, object.mime);
    stream.socket
      .binary(true)
      .emit(`${capabilityEventPath}/initialization`, object.initialization);
  });

  stream.fragmenter.on('segment', buffer => {
    stream.socket.binary(true).emit(`${capabilityEventPath}/segment`, buffer);
  });

  stream.source.on('error', stop);
  stream.mux.on('error', stop);
  stream.fragmenter.on('error', stop);

  stream.source.on('close', stop);
  stream.mux.on('close', stop);

  stream.source.stdout.pipe(stream.mux.stdin);
  stream.mux.stdout.pipe(stream.fragmenter);

  stream.isStreaming = true;

  return stream;
};

exports.initialize = socket => () => {
  stream.socket = socket;
  if (stream.socket) {
    // Attach event listeners to start video on demand.
    stream.socket.on(`${capabilityEventPath}/start`, start);
    stream.socket.on(`${capabilityEventPath}/stop`, stop);
  }
};

exports.terminate = () => {
  stop();
  if (stream.socket) {
    // Detach event listeners to prevent memory leaks.
    stream.socket.off(`${capabilityEventPath}/start`, start);
    stream.socket.off(`${capabilityEventPath}/stop`, stop);
  }
  stream.socket = null;
};
