const { tmpdir } = require('os');
const { readFile, writeFile, mkdir } = require('./fs');
const logger = require('./logger');

const memstoreFolder = `${tmpdir()}/labcloud-node-linux`;
const memstoreFile = `${memstoreFolder}/memstore.json`;

exports.set = async (key, value) => {
  // Create memstore directory.
  try {
    await mkdir(memstoreFolder, { recursive: true });
  } catch (err) {
    // Ignore if directory already exists.
  }

  // Read file and parse JSON.
  let object = null;
  try {
    const jsonString = await readFile(memstoreFile, 'utf8');
    object = JSON.parse(jsonString);
  } catch (err) {
    logger.warn(`Could not read JSON file: ${memstoreFile}`);
    logger.info(`Creating memstore file: ${memstoreFile}`);
  }

  // Write key to object.
  if (!object) {
    object = {
      [key]: value
    };
  } else {
    object[key] = value;
  }

  // Write changes to disk.
  try {
    await writeFile(memstoreFile, JSON.stringify(object, null, 2));
  } catch (err) {
    logger.error(`Setting key failed: ${key}`);
  }
};

exports.get = async key => {
  // Read file and parse JSON.
  let object = null;
  try {
    const jsonString = await readFile(memstoreFile, 'utf8');
    object = JSON.parse(jsonString);
  } catch (err) {
    logger.warn(`Could not read JSON file: ${memstoreFile}`);
  }
  return (object && object[key]) || undefined;
};

exports.reset = async key => {
  await exports.set(key, undefined);
};
