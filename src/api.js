const { get } = require('lodash');
const axios = require('axios');
const dotenv = require('dotenv');
const logger = require('./logger');

dotenv.config();

const clientOptions = {
  baseURL: `${process.env.API_URL}/v1`,
  headers: {
    'content-type': 'application/json'
  }
};
const client = axios.create(clientOptions);

const handleErrorMessage = err => {
  const errorMessage =
    get(err, 'response.data.error.message', '') ||
    get(err, 'response.data.error', '') ||
    err.message;
  logger.error(errorMessage.trim());
};

// Creates or reads a node in the API. Returns a
// node object or null in case of network errors.
exports.registerNode = async (entity = {}) => {
  let nodeId = '';
  try {
    const response = await client.post('/nodes', entity);
    return get(response, 'data.data', null);
  } catch (err) {
    nodeId = get(err, 'response.data.id', '');
    if (!nodeId) {
      handleErrorMessage(err);
      return null;
    }
  }
  try {
    const response = await client.get(`/nodes/${nodeId}`);
    return get(response, 'data.data', null);
  } catch (err) {
    handleErrorMessage(err);
    return null;
  }
};

exports.readNode = async (nodeId, populate = []) => {
  try {
    const endpoint = populate.length
      ? `/nodes/${nodeId}?${populate.join(',')}`
      : `/nodes/${nodeId}`;
    const response = await client.get(endpoint);
    return get(response, 'data.data', null);
  } catch (err) {
    handleErrorMessage(err);
    return null;
  }
};

exports.resumeSession = token => {
  client.defaults.headers.common.authorization = `Bearer ${token}`;
};

exports.createSession = async nodeId => {
  try {
    const response = await client.post(`/nodes/${nodeId}/sessions`);
    const session = get(response, 'data.data', null);
    client.defaults.headers.common.authorization = `Bearer ${session.token}`;
    return session;
  } catch (err) {
    handleErrorMessage(err);
    return null;
  }
};

exports.readSession = async (nodeId, sessionId) => {
  try {
    const response = await client.get(`/nodes/${nodeId}/sessions/${sessionId}`);
    return get(response, 'data.data', null);
  } catch (err) {
    handleErrorMessage(err);
    return null;
  }
};

exports.readPermission = async permissionId => {
  try {
    const response = await client.get(`/permissions/${permissionId}`);
    return get(response, 'data.data', null);
  } catch (err) {
    handleErrorMessage(err);
    return null;
  }
};

exports.readUser = async userIdOrTagId => {
  try {
    const response = await client.get(`/users/${userIdOrTagId}`);
    return get(response, 'data.data', null);
  } catch (err) {
    handleErrorMessage(err);
    return null;
  }
};

exports.readMachine = async machineId => {
  try {
    const response = await client.get(`/machines/${machineId}`);
    return get(response, 'data.data', null);
  } catch (err) {
    handleErrorMessage(err);
    return null;
  }
};

exports.updateMachine = async (machineId, entity) => {
  try {
    const response = await client.patch(`/machines/${machineId}`, entity);
    return get(response, 'data.data', null);
  } catch (err) {
    handleErrorMessage(err);
    return null;
  }
};

exports.createTool = async entity => {
  try {
    const response = await client.post(`/tools`, entity);
    return get(response, 'data.data', null);
  } catch (err) {
    handleErrorMessage(err);
    return null;
  }
};

exports.readTool = async toolIdOrTagId => {
  try {
    const response = await client.get(`/tools/${toolIdOrTagId}`);
    return get(response, 'data.data', null);
  } catch (err) {
    handleErrorMessage(err);
    return null;
  }
};

exports.updateTool = async (toolId, entity) => {
  try {
    const response = await client.patch(`/tools/${toolId}`, entity);
    return get(response, 'data.data', null);
  } catch (err) {
    handleErrorMessage(err);
    return null;
  }
};
