// eslint-disable-next-line import/no-unresolved
const pigpio = require('pigpio');

// Initialize library to make it play nicely with signal handlers.
pigpio.initialize();

const relay = new pigpio.Gpio(23, { mode: pigpio.Gpio.OUTPUT });
const toolInitButton = new pigpio.Gpio(24, {
  mode: pigpio.Gpio.INPUT,
  pullUpDown: pigpio.Gpio.PUD_UP
});

// Ensure relay is disabled by default.
relay.digitalWrite(0);

exports.enableRelay = () => {
  relay.digitalWrite(1);
};

exports.disableRelay = () => {
  relay.digitalWrite(0);
};

exports.getToolInitButton = () => {
  return !toolInitButton.digitalRead();
};

exports.terminate = () => {
  relay.digitalWrite(0);
  pigpio.terminate();
};
