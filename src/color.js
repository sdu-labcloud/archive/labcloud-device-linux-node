/* eslint-disable no-bitwise */
const rgbToBin = (r, g, b) =>
  ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
/* eslint-enable no-bitwise */

const colors = {
  black: rgbToBin(0, 0, 0),
  orange: rgbToBin(255, 100, 0),
  red: rgbToBin(255, 0, 0),
  green: rgbToBin(0, 255, 0),
  blue: rgbToBin(0, 0, 255),
  purple: rgbToBin(255, 0, 200),
  violet: rgbToBin(200, 0, 255),
  yellow: rgbToBin(255, 200, 0),
  white: rgbToBin(255, 255, 255)
};

module.exports = color => colors[color] || colors.black;
