const hub = require('./hub');
const logger = require('./logger');
const led = require('./led');
const system = require('./system');
const api = require('./api');
const rfid = require('./rfid');
const gpio = require('./gpio');
const { delay } = require('./util');
const memstore = require('./memstore');
const videostream = require('./videostream');

// Reset GPIOs.
const reset = (exitCode = 0) => {
  led.terminate();
  gpio.terminate();
  process.exit(exitCode);
};

// Main loop.
(async () => {
  // Disable ESLint rule "no-await-in-loop" as work cannot be parallelized.
  // Reference: https://eslint.org/docs/rules/no-await-in-loop
  /* eslint-disable no-await-in-loop */
  // eslint-disable-next-line no-constant-condition
  while (true) {
    // Indicate maintenance while booting up.
    led.static('orange');

    // Obtain system information.
    const nodeInfo = {
      mac_address: '',
      serial_number: '',
      operating_system: 'linux'
    };
    const serialPromise = system.getSerial();
    const iface = await system.getActiveNetworkInterface();
    const rawMacPromise = system.getMacAddress(iface);
    const rawMac = await rawMacPromise;
    nodeInfo.serial_number = await serialPromise;
    nodeInfo.mac_address = rawMac.replace(/:/g, '').toUpperCase();

    // Ensure that node exists in database.
    let node = await api.registerNode(nodeInfo);
    while (!node) {
      await delay(5000);
      node = await api.registerNode(nodeInfo);
    }
    logger.info(`Registered node: ${node.id}`);

    // Attempt to resume session.
    let session = null;
    const cachedSession = await memstore.get('session');
    if (cachedSession) {
      session = await api.readSession(node.id, cachedSession.id);
    }

    if (session) {
      // Load authorization token into API client.
      api.resumeSession(cachedSession.token);
      logger.info(`Resumed session: ${session.id}`);
    } else {
      // Create new session to get authentication token.
      session = await api.createSession(node.id);
      while (!session) {
        await delay(5000);
        session = await api.createSession(node.id);
      }
      logger.info(`Created session: ${session.id}`);
      // Persist session between daemon restarts.
      await memstore.set('session', session);
      logger.info(`Persisted session: ${session.id}`);
    }

    // Wait for session to be authorized.
    while (!session.authorized) {
      // Make it possible to identify sessions.
      if (session.identify) {
        led.blink('violet', 'black', 1);
      } else {
        led.blink('orange', 'black', 1);
      }
      await delay(1000);
      session = await api.readSession(node.id, session.id);
    }
    logger.info(`Obtained authorized session: ${session.id}`);
    led.static('orange');

    // Update node information in background.
    let nodeUpdater = setInterval(async () => {
      const updatedNode = await api.readNode(node.id);
      node = updatedNode || node;
    }, 5000);

    // Track status of active features.
    const capabilityStatus = {
      camera: false
    };
    let machine = null;
    let socket = null;

    while (node) {
      if (node.capabilities) {
        // Check if camera is installed and not already activated.
        if (node.capabilities.includes('raspberry_pi_camera')) {
          if (!capabilityStatus.camera) {
            // Reuse existing connection or create new one if not already connected.
            socket = hub.connect(session.token);

            // Called on successful connection and reconnection.
            socket.on('connect', videostream.initialize(socket));

            // Called on disconnect.
            socket.on('disconnect', videostream.terminate);

            // Prevent from connecting
            capabilityStatus.camera = true;
          }
        } else if (capabilityStatus.camera) {
          // Reset socket as it is no longer needed.
          socket = null;

          // Terminate videostream to free up CPU resouce.
          videostream.terminate();

          // Reset camera status.
          capabilityStatus.camera = false;
        }
      }

      if (node.roles) {
        // Check if node will be used for device management.
        if (node.roles.includes('device_management')) {
          if (node.machine) {
            // Read machine from API and ensure that status is ready.
            machine = await api.readMachine(node.machine);
            while (machine.status !== 'ready') {
              const updatedMachine = await api.updateMachine(machine.id, {
                status: 'ready'
              });
              if (updatedMachine && updatedMachine.status === 'ready') {
                // Indicate that provisioning is done.
                led.static('white');
              }
              machine = updatedMachine || machine;
              await delay(1000);
            }

            // Read UID from RFID tag.
            const rfidTagId = await rfid.getUid();
            if (rfidTagId) {
              logger.info(`Sensed RFID tag: ${rfidTagId}`);

              // Read permission from API.
              const permission = await api.readPermission(machine.permission);
              if (permission) {
                const { workflow, scheduling } = permission;

                // Business logic for devices that require permanent card presence and no approval.
                if (workflow === 'open' && scheduling === 'on_demand') {
                  // Read user from API.
                  const user = await api.readUser(rfidTagId);

                  // Check if user is registered.
                  if (user) {
                    logger.info(`User found: ${user.email}`);
                    // Check if user has required priviledges.
                    if (user.permissions.includes(permission.id)) {
                      // Enable access.
                      led.static('green');
                      gpio.enableRelay();
                      logger.info(`Granted access.`);

                      // Update machine status.
                      while (machine.status !== 'busy') {
                        const updatedMachine = await api.updateMachine(
                          machine.id,
                          {
                            status: 'busy'
                          }
                        );
                        machine = updatedMachine || machine;
                        await delay(1000);
                      }

                      // Wait for tag to be removed.
                      await rfid.isTagRemovedOrChanged(rfidTagId);

                      // Disable access.
                      led.static('white');
                      gpio.disableRelay();
                      logger.info(`Revoked access.`);

                      // Update machine status.
                      while (machine.status !== 'ready') {
                        const updatedMachine = await api.updateMachine(
                          machine.id,
                          {
                            status: 'ready'
                          }
                        );
                        machine = updatedMachine || machine;
                        await delay(1000);
                      }
                    } else {
                      logger.info('User does not have sufficient permissions.');
                      led.static('red');
                      await delay(500);
                    }
                  } else {
                    logger.info(`Unknown RFID tag ID: ${rfidTagId}`);
                    led.static('red');
                    await delay(500);
                  }
                }
              }
            } else {
              led.static('white');
              await delay(500);
            }
          } else {
            led.blink('orange', 'black', 1);
            await delay(500);
          }
        } else if (node.roles.includes('asset_management')) {
          const loopDelayMs = 500;
          const timeoutDelaySec = 10;
          const maxTimeoutCount = (timeoutDelaySec * 1000) / loopDelayMs;
          let adminMode = false;
          let toolInitButton = gpio.getToolInitButton();
          let timeoutCounter = 0;
          let user = null;
          let tool = null;

          // Use node instead of while true to make it possible to leave loop in case of unforeseen events.
          while (node) {
            // Indicate running status.
            led.static('white');

            // Increment timeout counter.
            if (user || tool) {
              timeoutCounter += 1;
            }

            // Check if timeout was exceeded.
            if (timeoutCounter > maxTimeoutCount) {
              user = null;
              tool = null;
              logger.info('Timeout expired.');
              logger.info('Resetting cached information.');

              // Reset idle timeout.
              timeoutCounter = 0;

              // Indicate successful automatic logout.
              led.static('black');

              await delay(loopDelayMs);
              // eslint-disable-next-line no-continue
              continue;
            }

            // Check if tool initialization button is pressed.
            toolInitButton = gpio.getToolInitButton();
            if (toolInitButton && user) {
              if (!['admin', 'staff'].includes(user.role)) {
                // Indicate status.
                led.static('red');
                await delay(loopDelayMs);
                // eslint-disable-next-line no-continue
                continue;
              }

              // Activate admin mode.
              adminMode = true;
              // Create variable for rising edge detection.
              let lastToolInitButtonValue = gpio.getToolInitButton();

              while (adminMode) {
                // Indicate admin mode.
                led.static('blue');

                // Read tool init button.
                lastToolInitButtonValue = toolInitButton;
                toolInitButton = gpio.getToolInitButton();

                // Check if button has been pressed again (via rising edge detection).
                if (!lastToolInitButtonValue && toolInitButton) {
                  // Disable admin mode.
                  adminMode = false;

                  await delay(loopDelayMs);
                  // eslint-disable-next-line no-continue
                  continue;
                }

                const rfidTagId = await rfid.getUid();
                if (rfidTagId === user.card_id) {
                  await delay(loopDelayMs);
                  // eslint-disable-next-line no-continue
                  continue;
                }

                // Write tag if it is present and create new tool.
                if (rfidTagId) {
                  // String that contains organisational information about the tools as hexadecimal string:
                  // LabCloud
                  // V.: 1.0.0
                  // SDU - The Core
                  // Alsion 2
                  // Sønderborg
                  const tagData =
                    '024C6162436C6F75640000000000000002562E3A20312E302E3000000000000002534455202D2054686520436F72650002416C73696F6E2032000000000000000253C3B86E646572626F726700000000';

                  // Write to RFID B1 module memory.
                  const dataBufferStatus = await rfid.writeDataBuffers(tagData);
                  // Write from module memory to RFID tag memory.
                  const dataBlockStatus = await rfid.writeDataBlocks(1, 5);
                  // Check written data.
                  const writtenTagData = await rfid.readDataBlocks(1, 5);
                  // Check if error occurred.
                  if (
                    !dataBufferStatus ||
                    !dataBlockStatus ||
                    writtenTagData !== tagData
                  ) {
                    logger.error('Writing data to tag failed.');
                    led.static('red');

                    await delay(500);
                    // eslint-disable-next-line no-continue
                    continue;
                  }

                  logger.info(`Wrote data to tag: ${rfidTagId}`);

                  const newTool = await api.createTool({
                    tag_id: rfidTagId
                  });

                  if (newTool && newTool.tag_id && newTool.id) {
                    logger.info('Added tool to database.');

                    // Indicate status.
                    led.static('green');

                    await delay(loopDelayMs);
                    // eslint-disable-next-line no-continue
                    continue;
                  }

                  logger.error('Adding tool to database failed.');

                  // Indicate status.
                  led.static('red');
                }

                await delay(loopDelayMs);
              }
            }

            // Check if a tool has a tool type, is therefore fully initialized, and is currently booked.
            if (tool && !tool.available && tool.type) {
              // If the tool is not available, thus currently booked, check it back in.
              tool = await api.updateTool(tool.id, {
                available: true,
                borrowed_by: null
              });

              if (tool && tool.available) {
                logger.info(`Checked tool in: ${tool.id}`);

                // Clear tool object.
                tool = null;

                // Indicate status.
                led.static('green');
              } else {
                logger.error(`Checking tool in failed.`);

                // Indicate status.
                led.static('red');
              }

              await delay(loopDelayMs);
              // Reset idle timeout.
              timeoutCounter = 0;
              // eslint-disable-next-line no-continue
              continue;
            }

            if (user && tool && tool.available) {
              // Check the tool out.
              tool = await api.updateTool(tool.id, {
                available: false,
                borrowed_by: user.id,
                borrowed_since: new Date().toISOString()
              });

              if (tool && !tool.available) {
                logger.info(`Checked tool out: ${tool.id}`);

                // Reset tool object.
                tool = null;

                // Indicate status.
                led.static('green');
              } else {
                logger.error(`Checking tool out failed.`);

                // Indicate status.
                led.static('red');
              }

              await delay(loopDelayMs);
              // eslint-disable-next-line no-continue
              continue;
            }

            const rfidTagId = await rfid.getUid();
            if (rfidTagId) {
              logger.info(`Sensed RFID tag: ${rfidTagId}`);
              const tagDataBlock1 = await rfid.readDataBlocks(1, 1);
              const tagDataString = Buffer.from(tagDataBlock1, 'hex')
                .toString('utf8')
                // Disable ESLint rule as the control characters are part of the written data.
                // eslint-disable-next-line no-control-regex
                .replace(/(\u0000|\u0002)+/g, '');
              // Check if tag is a tool or a student card.
              if (tagDataString === 'LabCloud') {
                // Tag is a (registered) tool.
                tool = await api.readTool(rfidTagId);

                // Check if tool was deleted in the database.
                if (tool) {
                  logger.info(`Tool found: ${tool.id}`);
                  // Indicate status.
                  led.static('green');
                  await delay(loopDelayMs);
                  // Reset idle timeout.
                  timeoutCounter = 0;
                  // eslint-disable-next-line no-continue
                  continue;
                } else {
                  // Indicate status.
                  led.static('red');
                  await delay(loopDelayMs);
                  // Reset idle timeout.
                  timeoutCounter = 0;
                  // eslint-disable-next-line no-continue
                  continue;
                }
              } else {
                // Tag is a student card or unknown.
                user = await api.readUser(rfidTagId);

                // Check if user exists or tag is unknown.
                if (user) {
                  logger.info(`User found: ${user.id}`);
                  // Indicate status.
                  led.static('green');
                  await delay(loopDelayMs);
                  // Reset idle timeout.
                  timeoutCounter = 0;
                  // eslint-disable-next-line no-continue
                  continue;
                } else {
                  // Indicate status.
                  led.static('red');
                  await delay(loopDelayMs);
                  // Reset idle timeout.
                  timeoutCounter = 0;
                  // eslint-disable-next-line no-continue
                  continue;
                }
              }
            }

            await delay(loopDelayMs);
          }
        } else {
          led.blink('orange', 'black', 1);
          await delay(500);
        }
      } else {
        led.blink('orange', 'black', 1);
        await delay(500);
      }
    }

    if (nodeUpdater) {
      clearInterval(nodeUpdater);
      nodeUpdater = null;
    }
  }
  // eslint-enable no-await-in-loop
})().catch(err => {
  logger.error(`Unknown error: ${err.message}\n${err.stack}`);
  reset(1);
});

// Trap the termination signals and reset before exit.
process.on('SIGINT', reset);
process.on('SIGTERM', reset);
process.on('SIGCONT', reset);
process.on('SIGHUP', reset);
