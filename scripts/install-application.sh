#!/usr/bin/env bash

# Global variables.
appName="labcloud-node-linux"
repoUrl="https://gitlab.com/sdu-labcloud/${appName}.git"
installDir="/opt/${appName}"
installUser="root"

# Color variables.
default='\e[00;0m'
red='\e[00;31m'
green='\e[00;32m'
magenta='\e[00;35m'

# Color shortcuts.
d=${default}
r=${red}
g=${green}
m=${magenta}

# Message indicators.
info="${g}info:${d}   "
error="${r}error:${d}  "

# Check if user has sudo rights.
if [[ ${EUID} -ne 0 ]]; then
  echo -e "${error}This script must be run as ${m}root${d} or with ${m}sudo${d}."
  exit 1
fi

# Display error and exit program in case a command failed.
# First parameter is the return code of a program,
# which can be obtained by $? and the second parameter
# is a message in case the return code is non-zero.
# Usage: check_error $? "Something went wrong."
check_error() {
  if [ ${1} -ne 0 ]; then
    echo -e "${error}${2}"
    exit 1
  fi
}

# Update operating system.
echo -e "${info}Updating system..."
apt-get update -y
check_error $? "Failed to update package lists."
apt-get dist-upgrade -y
check_error $? "Failed to perform upgrade of outdated packages."

# Install apt package dependencies.
echo -e "${info}Installing depedencies via apt..."
apt-get install -y git pigpio ffmpeg
check_error $? "Failed to install dependencies: ${m}git pigpio${d}"

# Clone application repository.
echo -e "${info}Cloning application repository..."
mkdir -p ${installDir}
chown -R ${installUser}:${installUser} ${installDir}
git clone ${repoUrl} ${installDir}
check_error $? "Failed to clone application repository: ${m}${repoUrl}${d}"
cp /tmp/usb-provisioning/environment.ini ${installDir}/.env
check_error $? "Failed to load environment variables."

# Install node via node version manager.
${installDir}/scripts/install-node.sh
check_error $? "Failed to install dependency: ${m}node${d}"

# Install dependencies via npm.
pushd ${installDir}
source "${HOME}/.nvm/nvm.sh"
rm package-lock.json
npm i --unsafe-perm
git reset --hard
popd

# Install rfidb1-tool.
${installDir}/scripts/install-rfidb1-tool.sh
check_error $? "Failed to install dependency: ${m}rfidb1-tool${d}"

# Install daemon.
echo -e "${info}Installing systemd service..."
cp "${installDir}/scripts/systemd/${appName}.service" "/lib/systemd/system/${appName}.service"
check_error $? "Failed to copy service definition: ${m}${appName}${d}"

# Enable and start daemon.
systemctl enable --now "${appName}"
check_error $? "Failed to enable service at startup: ${m}${appName}${d}"
echo -e "${info}Successfully installed application: ${m}${appName}${d}"
