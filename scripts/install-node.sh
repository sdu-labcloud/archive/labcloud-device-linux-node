#!/usr/bin/env bash

# Global variables.
nvmVersion="v0.34.0"
nvmUrl="https://raw.githubusercontent.com/nvm-sh/nvm/${nvmVersion}/install.sh"
tempDir="/tmp/nvm"

# Color variables.
default='\e[00;0m'
green='\e[00;32m'

# Color shortcuts.
d=${default}
g=${green}

# Message indicators.
info="${g}info:${d}   "
error="${r}error:${d}  "

# Display error and exit program in case a command failed.
# First parameter is the return code of a program,
# which can be obtained by $? and the second parameter
# is a message in case the return code is non-zero.
# Usage: check_error $? "Something went wrong."
check_error() {
  if [ ${1} -ne 0 ]; then
    echo -e "${error}${2}"
    exit 1
  fi
}

# Download and run node version manager (nvm) installation script.
echo -e "${info}Downloading and installing node version manager..."
mkdir -p "${tempDir}"
wget -nv -O "${tempDir}/install.sh" "${nvmUrl}"
check_error $? "Downloading install script failed."
chmod +x "${tempDir}/install.sh"
bash "${tempDir}/install.sh"

# Checkout on specific version of nvm.
pushd "${HOME}/.nvm"
check_error $? "Setting nvm version failed."
git checkout ${nvmVersion}
popd

# Load node version manager into shell.
export NVM_DIR="${HOME}/.nvm"
[ -s "${NVM_DIR}/nvm.sh" ] && source "${NVM_DIR}/nvm.sh"
check_error $? "Loading nvm failed."

# Install and use node dubnium LTS.
echo -e "${info}Installing node..."
nvm install --lts=dubnium
check_error $? "Installing node failed."
nvm use --lts=dubnium
check_error $? "Installing node failed."

# Remove temporary directory.
rm -rf "${tempDir}"

echo -e "${info}Successfully installed dependency: ${m}node${d}"
