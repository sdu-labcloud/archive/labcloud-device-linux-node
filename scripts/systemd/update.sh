#!/usr/bin/env bash

# Change to working directory.
pushd /opt/labcloud-node-linux

# Clean the application folder.
git reset --hard
git clean -fd

# Update the local application directory.
git pull

# Load node version manager.
source "${HOME}/.nvm/nvm.sh"

# Install the dependencies.
npm i --unsafe-perm

# Clean the application folder.
git reset --hard
git clean -fd

# Leave working directory.
popd /opt/labcloud-node-linux

# Restart service.
systemctl restart labcloud-node-linux
