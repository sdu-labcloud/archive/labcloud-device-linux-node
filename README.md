# LabCloud - Node Linux

A node to manage a device of the "LabCloud" manufacturing platform.

## Installation

Start out by creating a `.env` file in the project root with the following content:

```ini
API_URL=http://localhost:9000
POLL_INTERVAL=10000
```

Make sure to have at least the latest LTS of [nodejs](https://nodejs.org/en/) installed, then run:

```shell
$ npm i
```

Once you have the dependencies installed, you can run:

```shell
$ npm start
```

### Workflow

Before you provision your node, open the SD card in a file browser of your choice and create a file with the name `ssh` (no file extension) in the `boot` partition of the SD card. This will enable the SSH server, to which you can connect via the default credentials (user `pi` with password `raspberry`):

```shell
$ ssh pi@${PI_NETWORK_ADDRESS}
```

Afterwards, become root and navigate to the installation folder of the application:

```bash
# Start root terminal session.
$ sudo -s
# Go to application directory.
$ cd /opt/labcloud-node-linux
```

Now, make changes to the repository on your local computer and push them to a branch. Afterwards, you can test these on the Pi by running the following commands:

```bash
# Fetch latest changes.
$ git fetch
# Switch to the same branch to synchronize the version.
$ git checkout $BRANCH_NAME
# Reload service to apply changes.
$ systemctl restart labcloud-node-linux
```

Afterwards, you can just synchronize the changes from your development computer to the device by running:

```bash
# Fetch and merge latest changes.
$ git pull
# Reload service to apply changes.
$ systemctl restart labcloud-node-linux
```

## Linting

In order to verify the code, you can run [ESLint](https://eslint.org) by typing:

```shell
$ npm run lint
```

## Formatting

If you are not using a plugin for your IDE to automatically format the code with [Prettier](https://prettier.io), you can run it via:

```shell
$ npm run format
```

## Testing

To run the unit test suite of this application, run:

```shell
$ npm run test:unit
```

## License

This project is licensed under the terms of the [MIT License](https://mit-license.org).
